#include "debug.h"
#include <stdio.h>
#include <ctime>

int _debugging = DEBUG_OFF;

char * get_local_time()
{
	char time_buf[128];
	time_t rawtime;
	struct tm * timeinfo = NULL;

	time(&rawtime);
	localtime_s(timeinfo, &rawtime);
	asctime_s(time_buf, timeinfo);
	return time_buf;
}

void debug(char *msg)
{
	if(_debugging == DEBUG_ON) printf("%s -- %s\n", "----", msg);
}

void toggle_debugging()
{
	_debugging = (_debugging == DEBUG_ON) ? DEBUG_OFF : DEBUG_ON;
}