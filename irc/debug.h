#ifndef IRCBOT_DEBUG_H_
#define IRCBOT_DEBUG_H_

#define DEBUG_ON 0
#define DEBUG_OFF -1

void debug(char *msg);
void toggle_debugging();

#endif