
#include "bot.h"
#include "debug.h"

void log_error(const char * msg)
{
	printf(msg);
}

void cleanup()
{
	WSACleanup();
}

void cleanup_and_exit(int error_code)
{
	cleanup();
	exit(error_code);
}

int init_network(WSAData *wsaData, addrinfo *hints)
{
	int result_code = WSAStartup(MAKEWORD(2,2), wsaData);
	char msg_buf[50];
	
	if(result_code != 0)
	{
		_snprintf_s(msg_buf, sizeof(msg_buf), "WSAStartup failed: %d\n", result_code);
		log_error(msg_buf);
		cleanup_and_exit(IRC_ERROR_NETWORK_INIT);
	}

	/* Setup hints for setting up socket */
	hints->ai_family = AF_UNSPEC;
	hints->ai_socktype = SOCK_STREAM;
	hints->ai_protocol = IPPROTO_TCP;

	return result_code;
}

int connect_to_server(SOCKET socket, addrinfo *result)
{
	int result_code = 0;
	char msg_buf[50];

	if(result == 0) result_code = -99;

	result_code = connect(socket, 
					 result->ai_addr,
					 result->ai_addrlen);

	if(result_code == SOCKET_ERROR)
	{
		closesocket(socket);
		socket = INVALID_SOCKET;
		_snprintf_s(msg_buf, sizeof(msg_buf), "socket connect() failed: %d\n", result_code);
		log_error(msg_buf);
		freeaddrinfo(result);
		cleanup_and_exit(IRC_ERROR_SOCKET_CONNECT);
	}

	return result_code;
}

/* Return pointer to socket */
SOCKET create_socket(addrinfo *hints)
{
	struct addrinfo *result = NULL;
	SOCKET csocket = INVALID_SOCKET;
	int result_code;
	char msg_buf[50];

	result_code = getaddrinfo(HOSTNAME, DEFAULT_PORT, hints, &result);
	if(result_code != 0)
	{
		_snprintf_s(msg_buf, sizeof(msg_buf), "getaddrinfo failed: %d\n", result_code);
		log_error(msg_buf);
		cleanup_and_exit(IRC_ERROR_GETADDR_INFO);
	}
	
	csocket = socket(result->ai_family, 
					 result->ai_socktype, 
					 result->ai_protocol);
					 
	if(csocket == INVALID_SOCKET)
	{
		_snprintf_s(msg_buf, sizeof(msg_buf), "Error at socket(): %ld\n", WSAGetLastError());
		freeaddrinfo(result);
		log_error(msg_buf);
		cleanup_and_exit(IRC_ERROR_CREATE_SOCKET);
	}

	connect_to_server(csocket, result);

	return csocket;
}

SOCKET start_irc_bot() { 
	
	WSAData wsaData;
	/* Define default socket explictly */
	
	SOCKET socket = INVALID_SOCKET;

	struct addrinfo hints;
	/* Zero out memory for hints struct */
	ZeroMemory(&hints, sizeof(hints));

	/* Initialize network */
	init_network(&wsaData, &hints);

	/* Create a socket */
	socket = create_socket(&hints);
	
	_bot_is_ready = 0;

	return socket;
}

void stop_irc_bot(SOCKET socket)
{
	char msg_buf[50];
	int result_code = shutdown(socket, SD_BOTH);

	if(result_code == SOCKET_ERROR)
	{
		_snprintf_s(msg_buf, sizeof(msg_buf), "shutdown failed: %d\n", result_code);
		log_error(msg_buf);
		cleanup_and_exit(IRC_ERROR_NETWORK_INIT);
	}
	closesocket(socket);
}

int send_data(const char *data, SOCKET s)
{
	int len = strlen(data);
	int bytes_sent = send(s, data, len, 0);

	if (bytes_sent > 0)
		debug("Sent bytes");

	if(bytes_sent == 0)
		return -1;
	else
		return 0;
}

int send_privmsg(const char *msg, char *channel, SOCKET s)
{
	std::string msg_cmd;
	msg_cmd += "PRIVMSG ";
	msg_cmd +=  channel;
	msg_cmd += ' ';
	msg_cmd += msg;
	msg_cmd += "\r\n";
	
	return send_data(msg_cmd.data(), s);
}

void handle_msg(char *msg, SOCKET s)
{
	if(strstr(msg, "hi bot"))
	{
		debug("DETECTED");
		send_privmsg("Hello User", "#test", s);
	}

	if(strstr(msg, "kill yourself"))
	{
		_bot_is_listening = -1;
	}
}

void send_pong(char *buf, SOCKET s)
{
	char * to_search = "PING ";
	
	if(strstr(buf, to_search) != NULL)
	{
		char pong_msg[5];
		pong_msg[0] = 'P';
		pong_msg[1] = 'O';
		pong_msg[2] = 'N';
		pong_msg[3] = 'G';
		pong_msg[4] = ' ';

		send_data(pong_msg, s);
		debug("PING-PONG");
	}
}

void listen(SOCKET s)
{
	char msg_buf[50];
	if(_bot_is_ready != 0)  
	{
		_snprintf_s(msg_buf, sizeof(msg_buf), "Connection not ready\n");
		log_error(msg_buf);
		cleanup_and_exit(ERROR_NOT_READY);
	}
	
	int numbytes;
	char buf[DEFAULT_BUF_LEN];

	int _bot_is_listening = 0;
	int count = 0;

	debug("Listening...");
	while(_bot_is_listening == 0)
	{ /* BEGIN WHILE */
		count++;
		switch(count)
		{
			case 2:
				debug("Sending nick");
				send_data(DEFAULT_NICK, s);
				debug("Sending user");
				send_data(DEFAULT_USER, s);
				break;
			case 3:
				debug("Joining channel");
				send_data(DEFAULT_CONNECTION_STRING, s);
			default:
				break;
		}

		numbytes = recv(s, buf, DEFAULT_BUF_LEN-1, 0);
		if(numbytes > 0) debug("Received bytes");
		buf[numbytes] = '\0';
		printf("%s\n", buf);

		handle_msg(buf, s);

		/* If Ping received must reply otherwise connection will be closed */
		if(strstr(buf, "PING") != NULL)
		{
			send_pong(buf, s);
		}

		if(numbytes == 0 || _bot_is_listening != 0)
		{
			printf("---- Connection Closed ----\n");
			break; /* Break out of loop */
		}
	} /* END WHILE */
}

int main(int argc, char **argv)
{
	SOCKET socket;
	socket = start_irc_bot();

	listen(socket);

	stop_irc_bot(socket);
	cleanup_and_exit(IRC_SUCCESS);
	return 0;
}