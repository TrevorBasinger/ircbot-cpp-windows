
#ifndef IRCBOT_H_
#define IRCBOT_H_

#include <stdio.h>
#include <stdio.h>
#include <stdlib.h>
#include <string>

/* Network library includes */
#include <WinSock2.h>
#include <WS2tcpip.h>

#pragma comment(lib, "ws2_32.lib")

#define HOSTNAME "10.10.45.188"
#define DEFAULT_PORT "6667"
#define DEFAULT_BUF_LEN 512
#define DEFAULT_NICK "NICK blahbot\r\n"
#define DEFAULT_USER "USER BlahBot localhost.localdomain localhost blahbot\r\n"
#define DEFAULT_CONNECTION_STRING "JOIN #test\r\n"

/* Error Codes */
#define IRC_SUCCESS					 0
#define IRC_ERROR_NETWORK_INIT		-1
#define IRC_ERROR_GETADDR_INFO		-2
#define IRC_ERROR_CREATE_SOCKET		-3
#define IRC_ERROR_SOCKET_CONNECT	-4
#define IRC_ERROR_NOT_READY			-5

/* Set to 0 when bot is connected to server */
int _bot_is_ready = -1;
int _bot_is_listening = -1;

SOCKET create_socket(addrinfo hints);

SOCKET start_irc_bot();
void stop_irc_bot(SOCKET socket);

int send_data(const char *data, SOCKET s);
int send_privmsg(const char *msg, char *channel, SOCKET s);

void handle_msg(char *msg, SOCKET s);

#endif